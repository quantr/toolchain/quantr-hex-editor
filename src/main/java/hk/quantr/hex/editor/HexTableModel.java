package hk.quantr.hex.editor;

import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class HexTableModel extends AbstractTableModel {

    public int noOfColumn = 8;
    public byte[] bytes = new byte[0];
    public String type = "hex";

    @Override
    public int getColumnCount() {
        return noOfColumn;
    }

    @Override
    public int getRowCount() {
        return bytes.length / noOfColumn + 1;
    }

    @Override
    public String getColumnName(int col) {
        return String.valueOf(col);
    }

    @Override
    public Object getValueAt(int row, int col) {
        if (bytes.length == 0) {
            return null;
        }
        int index = row * noOfColumn + col;
        if (index < bytes.length) {
            if (type.equals("hex")) {
                return String.format("%02x", bytes[index]);
            } else if (type.equals("dec")) {
                return String.format("%d", bytes[index] & 0xff);
            } else if (type.equals("oct")) {
                return String.format("%o", bytes[index]);
            } else if (type.equals("bin")) {
                return String.format("%1$8s", Integer.toBinaryString(bytes[index] & 0xff)).replace(' ', '0');

            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public Class getColumnClass(int c) {
        return String.class;
    }

    @Override
    public boolean isCellEditable(int row, int col) {
        return false;
    }

}
